
test:
	go test -v ./... -count=1

run:
	go run cmd/exchange-api/main.go

tidy:
	go mod tidy
	go mod vendor