package rpc

import "gitlab.com/aghia7/exchangewrapper/pkg/models"

type RPC interface {
	GetOrderBook(*models.OrderBookRequest) (*models.OrderBookResponse, error)
}
