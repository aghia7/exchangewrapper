package models

type OrderPair struct {
	Price  float64 `json:"price"`
	Amount float64 `json:"amount"`
}

type OrderBookResponse struct {
	Bids []OrderPair `json:"bids"`
	Asks []OrderPair `json:"asks"`
}
